# ConnectFour
## By Chase Strickler
#### 28 January 2019

This repository holds a Python script that simulates the game Connect Four. To play the simulation, clone this repository, navigate to the folder in your terminal and type "python connectfour.py" to run.

If you have multiple versions of Python installed, you may have to type "python3 connectfour.py" to run.