from colorama import init
init()
from colorama import Fore, Back, Style

class ConnectFour:
    """
    The ConnectFour class defines the Connect 4 gamespace.
    It has attributes of rows and cols, which hold the number of each.

    There is also a col_conversion_dict attribute that converts the column letter input
    to a numerical value.
    """


    def __init__(self):
        self.rows   = 6
        self.cols   = 7
        self.playeronemoves  = 0
        self.playertwomoves  = 0
        self.playeronesymbol = "B"
        self.playertwosymbol = "W"
        self.placedrow       = 0
        self.placedcol       = 0

        self.board  = [
                        ["-", "-", "-", "-", "-", "-", "-"],
                        ["-", "-", "-", "-", "-", "-", "-"],
                        ["-", "-", "-", "-", "-", "-", "-"],
                        ["-", "-", "-", "-", "-", "-", "-"],
                        ["-", "-", "-", "-", "-", "-", "-"],
                        ["-", "-", "-", "-", "-", "-", "-"],
                      ]

        self.col_conversion_dict = {    "A" : 0,
                                        "B" : 1,
                                        "C" : 2,
                                        "D" : 3,
                                        "E" : 4,
                                        "F" : 5,
                                        "G" : 6
                                    }



    def welcome(self):
        """
        This function welcomes users to a new game of ConnectFour
        """
        print("")
        print('\t','\t', "+================================+")
        print('\t','\t', "|                                |")
        print('\t','\t', "|    Welcome to Connect Four!    |")
        print('\t', '\t',"|      By: Chase Strickler       |")
        print('\t','\t', "|                                |")
        print('\t', '\t',"+================================+")
        print("")




    def print_board(self):
        """
        This function will print the game board, and takes in a GameBoard object as input


        This is what the board should look like:

             A   B   C   D   E   F   G
        0  | - | - | - | - | - | - | - |
        1  | - | - | - | - | - | - | - |
        2  | - | - | - | - | - | - | - |
        3  | - | - | - | - | - | - | - |
        4  | - | - | - | - | - | - | - |
        5  | - | - | - | - | - | - | - |
        &%==============================%&


        """
        print(Style.RESET_ALL + '\t','\t', "     A   B   C   D   E   F   G")
        for row in range(len(self.board)):
            print(Style.RESET_ALL + '\t','\t', " ", row, end="")
            print("| ", end="")
            for col in range(len(self.board[row])):
                if self.board[row][col] is self.playeronesymbol:
                     print(Back.RED + self.board[row][col], end="")

                elif self.board[row][col] is self.playertwosymbol:
                    print(Back.YELLOW + self.board[row][col], end="")

                else:
                    print(Style.RESET_ALL + self.board[row][col], end="")
                print(Style.RESET_ALL + " | ", end="")
            print("")
        print('\t','\t', " &%=============================%&")
        print()



    def display_turn(self):
        """
        This function checks which player's turn it is, and also displays it.
        """
        if self.playeronemoves == self.playertwomoves: #Player One's Turn
            print("")
            print("    Player 1: %s <--" % (self.playeronesymbol))
            print("    Player 2: %s " % (self.playertwosymbol))
            print("")

            #This should really be moved and counted after the player makes a move

        else: #Player Two's turn
            print("")
            print("    Player 1: %s" % (self.playeronesymbol))
            print("    Player 2: %s <--" % (self.playertwosymbol))
            print("")

    def set_symbols(self):
        """
        This function allows the user to set what they would like their symbol to be.
        By default, player one is B and player two is W
        """
        print("")
        print("    Player 1, please pick what symbol you would like to represent you.")
        str = "Temporary"
        while len(str) != 1 or str == "-":
            error = 0

            if error > 0:
                print('\t', "          Invalid input!")

            error += 1
            print("          ", end="")
            str = input("   Your symbol must only be one character long: ")

        self.playeronesymbol = str

        print("")
        print("    Player 2, please pick what symbol you would like to represent you.")
        str = "Temporary"

        while len(str) != 1 or str == (self.playeronesymbol or "-"):
            error = 0

            if error > 0:
                print('\t', "          Invalid input!")

            error += 1
            print("          ", end="")
            str = input("   Your symbol must only be one character long: ")

        self.playertwosymbol = str


    def check_win(self):
        """
        This function checks if the last move on the board created a winning board.
        """

        #This section determines which player's turn it is.
        player = self.playeronesymbol

        if self.playeronemoves == self.playertwomoves and self.playeronemoves != 0:
            player = self.playertwosymbol

        win_counter = 0

        #Check horizontal victory from last placed move
        for col in range(self.cols):
            if self.board[self.placedrow][col] is player:
                win_counter += 1
                if win_counter == 4:
                    return True
            else:
                win_counter = 0
            if (self.cols - col + win_counter) < 4:
                 break

        #Check vertical victory from last placed move
        for row in range(self.rows):
            if self.board[row][self.placedcol] is player:
                win_counter += 1
                if win_counter == 4:
                    return True

            else:
                win_counter = 0

            if(self.rows - row + win_counter) < 4:
                break

        #Check Diagonal victory, slope of -1 on cartesian grid
        drow = 0
        dcol = 0

        if self.placedrow < self.placedcol:
            dcol = self.placedcol - self.placedrow
        else:
            drow = self.placedrow - self.placedcol

        win_counter = 0
        while drow < self.rows and dcol < self.cols:
            if self.board[drow][dcol] is player:
                win_counter +=1
                if win_counter == 4:
                    return True
            else:
                win_counter = 0
            drow += 1
            dcol += 1

        #Check Diagonal victory, slope of 1
        drow = self.placedrow
        dcol = self.placedcol

        while dcol < 6 and drow > 0:
            dcol += 1
            drow -= 1

        win_counter = 0
        while drow < self.rows and dcol > 0:
            if self.board[drow][dcol] is player:
                win_counter +=1
                if win_counter == 4:
                    return True
            else:
                win_counter = 0
            drow += 1
            dcol -= 1

        if win_counter == 4:
            return True
        else:
            return False

    def place_move(self, column):
        """
        Given a column, this function drops the piece into place on the grid.
        """
        #Convert letter input to a number
        col = self.col_conversion_dict.get(column)
        row = 0
        invalid = False
        if self.board[0][col] is not "-":
            print("    Invalid move: space is occupied!")
            invalid = True
        else:
            while self.board[row][col] is "-" and row < self.rows - 1:
                if self.board[row+1][col] is not "-":
                    break
                row += 1

        if invalid == False:
            #These two lines record the last placed move on the board.
            self.placedrow = row
            self.placedcol = col

            if self.playeronemoves > self.playertwomoves:
                self.board[row][col] = self.playertwosymbol
                self.playertwomoves += 1
            else:
                self.board[row][col] = self.playeronesymbol
                self.playeronemoves += 1

    def print_win(self, player):
        """
        This function announces the winner of the game
        """
        print("")
        print('\t','\t', "+================================+")
        print('\t','\t', "|                                |")
        print('\t','\t', "|         Player %s Wins!         |" % (player))
        print('\t','\t', "|                                |")
        print('\t', '\t',"+================================+")
        print("")


    def directions(self):
        """
        This function explains to the user the directions of the game.
        """
        print("")
        print("    Directions: To win Connect Four you must be the first player ")
        print("             to get four of your symbols in a row either horizontally,")
        print("             vertically or diagonally. Display the directions by")
        print("             typing help")
        print()


    def howtoplay(self):
        """
        This function explains to the user how to play the game.
        """
        print("")
        print("    How to Play: Type the column where you would like to place your move")
        print("             For example, typing 'A' during my turn will place my move")
        print("             in the most immediate available spot in the left most column.")
        print("             Move inputs must be uppercase. Display this information by")
        print("             typing help. Type 'quit' at anytime to exit the game.")
        print()

def main():
    """
    This is the main function to actually run the game.
    """
    play = True
    while play == True:
        game = ConnectFour()
        game.welcome()
        game.directions()
        game.howtoplay()
        game.set_symbols()

        while game.check_win() is False:
            game.display_turn()
            game.print_board()
            move = input("    Enter the column where you would like to place your move: ")
            print()
            while game.col_conversion_dict.get(move) is None:
                    if move == "help":
                        game.directions()
                        game.howtoplay()
                    if move == "quit": #Allows user to quit the game
                        play = False
                        exit()
                    move = input("    Enter the column where you would like to place your move: ")
                    print()
            game.place_move(move)

        if game.playeronemoves > game.playertwomoves:
            game.print_win("1")
        else:
            game.print_win("2")

        game.print_board()
        print()
        again = input("    Would you like to play again? (Y/N): ")
        print()
        if again == "Y":
            play = True
        else:
            play = False

if __name__ == '__main__':
    main()
